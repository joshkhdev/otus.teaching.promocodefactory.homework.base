﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T entity)
        {
            Data = Data.Append(entity);

            return Task.FromResult(entity);
        }

        public Task<bool> UpdateAsync(T entity)
        {
            var entityExists = Data.Any(e => e.Id == entity.Id);

            if (entityExists)
            {
                Data = Data.Where(e => e.Id != entity.Id).Append(entity);
            }

            return Task.FromResult(entityExists);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var entityExists = Data.Any(e => e.Id == id);

            if (entityExists)
            {
                Data = Data.Where(e => e.Id != id);
            }

            return Task.FromResult(entityExists);
        }
    }
}