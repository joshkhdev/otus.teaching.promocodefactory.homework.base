﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class UpdateEmployeeRequest
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public RoleItemShort[] Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
