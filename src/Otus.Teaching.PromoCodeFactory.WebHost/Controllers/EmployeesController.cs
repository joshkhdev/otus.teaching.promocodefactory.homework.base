﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(e => new EmployeeShortResponse(e)).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody] CreateEmployeeRequest createRequest)
        {
            var roles = await _roleRepository.GetAllAsync();

            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = createRequest.FirstName,
                LastName = createRequest.LastName,
                Email = createRequest.Email,
                Roles = createRequest.Roles.Select(role => roles.FirstOrDefault(r => r.Name == role.Name)).ToList(),
                AppliedPromocodesCount = 0,
            };

            var result = await _employeeRepository.AddAsync(employee);

            return new EmployeeResponse(employee);
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync([FromBody] UpdateEmployeeRequest updateRequest)
        {
            var roles = await _roleRepository.GetAllAsync();

            var employee = new Employee()
            {
                Id = updateRequest.Id,
                FirstName = updateRequest.FirstName,
                LastName = updateRequest.LastName,
                Email = updateRequest.Email,
                Roles = updateRequest.Roles.Select(role => roles.FirstOrDefault(r => r.Name == role.Name)).ToList(),
                AppliedPromocodesCount = updateRequest.AppliedPromocodesCount,
            };

            var success = await _employeeRepository.UpdateAsync(employee);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var success = await _employeeRepository.DeleteAsync(id);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}